<?php

require APPPATH . 'libraries/REST_Controller.php';

class Indonesia extends REST_Controller{

  // construct
  public function __construct(){
    parent::__construct();
    $this->load->model('Model_indonesia');
  }

  // method index untuk menampilkan semua data data_indonesia menggunakan method get
  public function index_get(){
    $response = $this->Model_indonesia->all_data_indonesia();
    $this->response($response);
  }

  // untuk menambah data_indonesia menaggunakan method post
  public function add_post(){
    $response = $this->Model_indonesia->add_data_indonesia(
        $this->post('negara'),
        $this->post('positif'),
        $this->post('sembuh'),
        $this->post('kematian')
      );
    $this->response($response);
  }

  // update data data_indonesia menggunakan method put
  public function update_put(){
    $response = $this->Model_indonesia->update_data_indonesia(
        $this->put('id'),
        $this->put('negara'),
        $this->put('positif'),
        $this->put('sembuh'),
        $this->put('kematian')
      );
    $this->response($response);
  }

  // hapus data data_indonesia menggunakan method delete
  public function delete_delete(){
    $response = $this->Model_indonesia->delete_data_indonesia(
        $this->delete('id')
      );
    $this->response($response);
  }

}

?>
