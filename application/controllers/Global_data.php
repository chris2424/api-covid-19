<?php

require APPPATH . 'libraries/REST_Controller.php';

class Global_data extends REST_Controller{

  // construct
  public function __construct(){
    parent::__construct();
    $this->load->model('Model_global');
  }

  // method index untuk menampilkan semua data data_global menggunakan method get
  public function index_get(){
    $response = $this->Model_global->all_data_global();
    $this->response($response);
  }

  // untuk menambah data_global menaggunakan method post
  public function add_post(){
    $response = $this->Model_global->add_data_global(
        $this->post('wilayah'),
        $this->post('cases'),
        $this->post('deaths'),
        $this->post('source'),
        $this->post('update_at')
      );
    $this->response($response);
  }

  // update data data_global menggunakan method put
  public function update_put(){
    $response = $this->Model_global->update_data_global(
        $this->put('id'),
        $this->put('wilayah'),
        $this->put('cases'),
        $this->put('deaths'),
        $this->put('source'),
        $this->put('update_at')
      );
    $this->response($response);
  }

  // hapus data data_global menggunakan method delete
  public function delete_delete(){
    $response = $this->Model_global->delete_data_global(
        $this->delete('id')
      );
    $this->response($response);
  }

}

?>
