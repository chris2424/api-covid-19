<?php

// extends class Model
class Model_indonesia extends CI_Model{

  // response jika field ada yang kosong
  public function empty_response(){
    $response['status']=502;
    $response['error']=true;
    $response['message']='Field tidak boleh kosong';
    return $response;
  }
  // mengambil semua data data
  public function all_data_indonesia(){

    $all = $this->db->get("tb_indonesia")->result();
    $response['status']=200;
    $response['error']=false;
    $response['data']=$all;
    return $response;

  }
  // function untuk insert data ke tabel tb_indonesia
  public function add_data_indonesia($negara,$positif,$sembuh,$kematian){

    if(empty($negara) || empty($positif) || empty($sembuh)|| empty($kematian)){
      return $this->empty_response();
    }else{
      $data = array(
        "negara"=>$negara,
        "positif"=>$positif,
        "sembuh"=>$sembuh,
        "kematian"=>$kematian

      );

      $insert = $this->db->insert("tb_indonesia", $data);

      if($insert){
        $response['status']=200;
        $response['error']=false;
        $response['message']='Data data ditambahkan.';
        return $response;
      }else{
        $response['status']=502;
        $response['error']=true;
        $response['message']='Data data gagal ditambahkan.';
        return $response;
      }
    }

  }



  // hapus data data
  public function delete_data_indonesia ($id){

    if($id == ''){
      return $this->empty_response();
    }else{
      $where = array(
        "id"=>$id
      );

      $this->db->where($where);
      $delete = $this->db->delete("tb_indonesia");
      if($delete){
        $response['status']=200;
        $response['error']=false;
        $response['message']='Data data dihapus.';
        return $response;
      }else{
        $response['status']=502;
        $response['error']=true;
        $response['message']='Data data gagal dihapus.';
        return $response;
      }
    }

  }

  // update data
  public function update_data_indonesia($id,$negara,$positif,$sembuh,$kematian){

    if($id == '' || empty($negara) || empty($positif) || empty($sembuh)|| empty($kematian)){
      return $this->empty_response();
    }else{
      $where = array(
        "id"=>$id
      );

      $set = array(
        "negara"=>$negara,
        "positif"=>$positif,
        "sembuh"=>$sembuh,
        "kematian"=>$kematian
      );

      $this->db->where($where);
      $update = $this->db->update("tb_indonesia",$set);
      if($update){
        $response['status']=200;
        $response['error']=false;
        $response['message']='Data data diubah.';
        return $response;
      }else{
        $response['status']=502;
        $response['error']=true;
        $response['message']='Data data gagal diubah.';
        return $response;
      }
    }

  }

}

?>
