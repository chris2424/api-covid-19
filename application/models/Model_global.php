<?php

// extends class Model
class Model_global extends CI_Model{

  // response jika field ada yang kosong
  public function empty_response(){
    $response['status']=502;
    $response['error']=true;
    $response['message']='Field tidak boleh kosong';
    return $response;
  }
  // mengambil semua data data
  public function all_data_global(){

    $all = $this->db->get("tb_global")->result();
    $response['status']=200;
    $response['error']=false;
    $response['data']=$all;
    return $response;

  }
  // function untuk insert data ke tabel tb_global
  public function add_data_global($wilayah,$cases,$deaths,$source,$update_at){

    if(empty($wilayah) || empty($cases) || empty($deaths)|| empty($source)|| empty($update_at)){
      return $this->empty_response();
    }else{
      $data = array(
        "wilayah"=>$wilayah,
        "cases"=>$cases,
        "deaths"=>$deaths,
        "source"=>$source,
        "update_at"=>$update_at

      );

      $insert = $this->db->insert("tb_global", $data);

      if($insert){
        $response['status']=200;
        $response['error']=false;
        $response['message']='Data data ditambahkan.';
        return $response;
      }else{
        $response['status']=502;
        $response['error']=true;
        $response['message']='Data data gagal ditambahkan.';
        return $response;
      }
    }

  }



  // hapus data data
  public function delete_data_global ($id){

    if($id == ''){
      return $this->empty_response();
    }else{
      $where = array(
        "id"=>$id
      );

      $this->db->where($where);
      $delete = $this->db->delete("tb_global");
      if($delete){
        $response['status']=200;
        $response['error']=false;
        $response['message']='Data data dihapus.';
        return $response;
      }else{
        $response['status']=502;
        $response['error']=true;
        $response['message']='Data data gagal dihapus.';
        return $response;
      }
    }

  }

  // update data
  public function update_data_global($id,$wilayah,$cases,$deaths,$source,$update_at){

    if($id == '' || empty($wilayah) || empty($cases) || empty($deaths)|| empty($source)|| empty($update_at)){
      return $this->empty_response();
    }else{
      $where = array(
        "id"=>$id
      );

      $set = array(
        "wilayah"   =>$wilayah,
        "cases"     =>$cases,
        "deaths"    =>$deaths,
        "source"    =>$source,
        "update_at" =>$update_at
      );

      $this->db->where($where);
      $update = $this->db->update("tb_global",$set);
      if($update){
        $response['status']=200;
        $response['error']=false;
        $response['message']='Data data diubah.';
        return $response;
      }else{
        $response['status']=502;
        $response['error']=true;
        $response['message']='Data data gagal diubah.';
        return $response;
      }
    }

  }

}

?>
